# Configuration file for the Sphinx documentation builder.

project = 'ska-sdp-tmlite-repository'
copyright = '2022, Stephen Ord, Rodrigo Tobar'
author = 'Stephen Ord, Rodrigo Tobar'
html_theme = 'sphinx_rtd_theme'
extensions = [
    'sphinx.ext.intersphinx',
]
intersphinx_mapping = {
    'ska-sdp-tmlite-server': (
        'https://developer.skao.int/projects/ska-sdp-tmlite-server/en/latest',
        None),
    'ska-telmodel': (
        'https://developer.skao.int/projects/ska-telmodel/en/latest',
        None),
}
