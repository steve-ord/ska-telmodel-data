Rationale
=========

This repository is meant to act as a storage backend
for the :doc:`SKA SDP TMLite REST server <ska-sdp-tmlite-server:index>`.
As such, it stores Telescope Model data
that is served to users of the TMLite REST server.
Thanks to the use of GitLab,
the management of versioning and authorization comes for free.

As a "lite" approach,
the TMLite server currently accesses this repository
in a read-only fashion.
In other words,
any updates to the contents of the repository
must be done separately from the use of the TMLite server.
As the packages evolve this might not be the case anymore.


This repository partially implements
the ideas exposed in `ADR-30 <https://jira.skatelescope.org/browse/ADR-30>`__,
with a GitLab repository acting as a backend store
for Telescope Model data.


Repository structure
--------------------

This repository contains a ``tmdata`` directory at the top level.
This directory forms the root of a hierarchy of directories
representing the individual components of the overall Telescope Model.

The individual components of the Telescope Model
consist on JSON files with the required data.
Each file must contain a single top-level JSON object.

For example, this could be found within the ``data`` directory::

  instrument
  instrument/ska1_low
  instrument/ska1_low/layout
  instrument/ska1_low/layout/data.json

This structure is then turned into a single JSON object,
with a ``model`` member as its root,
whose value will be
the serialisation of this directory structure.
In this serialisation,
files' contents are taken as-is, and therefore represent a single object.
Directories are turned into members of the object
represented by their parent directory,
with the directory name as the member key,
and the directory JSON contents
as the member value.
The value of a directory is either an object with sub-members,
if the directory contains other sub-directories,
or the value of the ``data.json`` file
living in that directory.
Any additional files in a directory are ignored.

In the example above,
the resulting single JSON object will be::

 {
   "model": {
     "instrument": {
       "ska1_low": {
         "layout": {
           // the contents of data.json
         }
       }
     }
   }
 }

Note that this structure is very simplistic,
and might change in the future
as more features are supported.


Schemas
-------

It is the intention
that the individual components making up the Telescope Model
follow agreed-on JSON schemas,
in particular those living in the :doc:`ska-telmodel:index`.

For the time being there is no schema validation steps in place
when files are added to the repository.
This might change in the future
once schemas are re-considered more in full.


Access
------

Access to the data in this repository is public for reading,
but writing is limited to certain users.
Dynamically updating files in this repository via the TMLite server
is currently not implemented or allowed.
In other words,
any changes performed in the TMLite server
are ephemeral to the instance currently running,
and are in no way synchronised back to this repository.
