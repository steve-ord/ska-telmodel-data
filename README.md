# Prototype Telescope Model Repository

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-sdp-tmlite-repository/badge/?version=latest)](https://developer.skao.int/projects/ska-sdp-tmlite-repository/en/latest/?badge=latest)

Initial prototype repository for Telescope Model. It is not envisaged that this is the ultimate location for this. It will initially just hold those products required for the SDP workflows.

Contents:

prototype_model.json: A JSON format file of the current model - this is the ground truth at the moment.

data/...: Used as a store for larger products that can be accessed via the RESTful gitlab interface




