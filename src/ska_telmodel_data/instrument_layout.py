"""
Code for maintaining instrument layouts telescope model data -
i.e. synchronise it with primary inputs.
"""

import json

import numpy as np
import pykml.parser
from astropy.coordinates import EarthLocation

dtype_pos = np.dtype(
    [("name", "U10"), ("lon", "f8"), ("lat", "f8"), ("height", "f8")]
)


def read_layout_np(filename):
    """
    Read layout data in textual numpy format.
    """
    pos = np.loadtxt(filename, usecols=(0, 1, 2, 3), dtype=dtype_pos)
    assert (
        pos[0]["name"] == "Centre"
    ), "First entry in layout file must be the centre of the array"
    return pos


def read_layout_np2(filename):
    """
    Read layout data in textual numpy format.
    """
    pos = np.loadtxt(filename, usecols=(1, 3, 2, 4), dtype=dtype_pos)
    assert (
        pos[0]["name"] == "AC"
    ), "First entry in layout file must be the centre of the array"
    return pos


def read_layout_kml(filename):
    """
    Find placemarks in KML (Keyhole Markup language) and return them as
    (name, lon, lat) tuples
    """

    with open(filename, "r", encoding="utf8") as kml_file:
        doc = pykml.parser.parse(kml_file)
    pos = []
    for fld in doc.iterfind(
        "./{http://www.opengis.net/kml/2.2}Document/"
        "{http://www.opengis.net/kml/2.2}Folder"
    ):
        print(f"Folder {fld.name}")
        for placemark in fld.iterfind(
            ".//{http://www.opengis.net/kml/2.2}Placemark"
        ):
            try:
                # We are ignoring the elevation at the moment
                long, lat, height = str(
                    placemark["Point"]["coordinates"]
                ).split(",")
                pos.append(
                    (
                        str(placemark["name"]),
                        float(long),
                        float(lat),
                        float(height),
                    )
                )
            except AttributeError:
                pass
    pos = np.array(pos, dtype=dtype_pos)
    return pos


def read_layout(filename):
    """
    Read layout data from a file (numpy or KML)

    Returns a map of receptor name to (lat,lon,height) tuple.
    """
    if filename.endswith(".kml"):
        print(f"Reading KML from {filename}...")
        apos = read_layout_kml(filename)
    elif filename.endswith(".dat"):
        print(f"Reading CSV from {filename}...")
        apos = read_layout_np2(filename)
    else:
        print(f"Reading CSV from {filename}...")
        apos = read_layout_np(filename)
    print(f" ... {len(apos)} receptors")

    apos_by_name = {ap["name"]: ap for ap in apos}

    return apos_by_name


def earthloc_distance(loc1, loc2):
    """
    Determine distance between two earth locations
    """
    xx1, yy1, zz1 = loc1.to_geocentric()
    xx2, yy2, zz2 = loc2.to_geocentric()
    return np.sqrt((xx1 - xx2) ** 2 + (yy1 - yy2) ** 2 + (zz1 - zz2) ** 2)


def update_receptor(receptor, apos_by_name):
    """
    Update receptor position given an antenna position map
    :returns: Whether any changes were made
    """

    # Look up station position
    name = receptor["station_name"]
    apos = apos_by_name.get(name)
    if apos is None:
        # Try with "R" appended
        apos = apos_by_name.get(name + "R")
        if apos is None:
            print(f"Could not find position for station {name} - ignoring!")
            return False
        print(
            f"Could not find position for station {name} -"
            f"using {name}R instead"
        )
    ellipsoid = "WGS84"
    loc = EarthLocation.from_geodetic(apos["lon"], apos["lat"], apos["height"])

    # Add geodetic coordinates
    changes = False
    if "location" not in receptor:
        receptor["location"] = {}
    if "geodetic" not in receptor["location"]:
        print(f" adding geodetic coordinates for {name}")
        receptor["location"]["geodetic"] = {
            "interface": "https://schema.skao.int/"
            "ska-telmodel-layout-location-geodetic/0.0",
            "coordinate_frame": ellipsoid,
            "lat": float(apos["lat"]),
            "lon": float(apos["lon"]),
            "h": float(apos["height"]),
        }
    else:
        geodetic = receptor["location"]["geodetic"]
        old = EarthLocation.from_geodetic(
            geodetic["lon"], geodetic["lat"], geodetic["h"]
        )
        dist = earthloc_distance(old, loc)
        if dist.to("m").value >= 1e-3 or isinstance(geodetic["lat"], str):
            print(f" moving geodetic coordinates for {name} by {dist}")
            geodetic["coordinate_frame"] = ellipsoid
            geodetic["lat"] = apos["lat"]
            geodetic["lon"] = apos["lon"]
            geodetic["h"] = apos["height"]
            changes = True

    # Convert + add geocentric coordinates
    if "geocentric" not in receptor["location"]:
        receptor["location"]["geocentric"] = {
            "interface": "https://schema.skao.int/"
            "ska-telmodel-layout-location-geocentric/0.0",
            "coordinate_frame": "ITRF",
            "x": float(loc.x.to("m").value),
            "y": float(loc.y.to("m").value),
            "z": float(loc.z.to("m").value),
        }
    else:
        geocentric = receptor["location"]["geocentric"]
        old = EarthLocation.from_geocentric(
            geocentric["x"], geocentric["y"], geocentric["z"], unit="m"
        )
        dist = earthloc_distance(old, loc)
        if dist.to("m").value > 1e-3:
            print(f" moving geocentric coordinates for {name} by {dist}")
            geocentric["x"] = float(loc.x.to("m").value)
            geocentric["y"] = float(loc.y.to("m").value)
            geocentric["z"] = float(loc.z.to("m").value)
            changes = True

    return changes


def update_receptors_in_file(file_name, apos_by_name, dry_run=False):
    """
    Update receptor position given an antenna position map
    :returns: Whether any changes (would) have been written
    """

    # Read original file
    print(f"Reading receptors from {file_name}...")
    with open(file_name, encoding="utf8") as layout_file:
        contents = json.load(layout_file)

    # Loop through contained receptors
    changes = False
    for receptor in contents["receptors"]:
        if update_receptor(receptor, apos_by_name):
            changes = True

    if changes and not dry_run:
        print("Rewriting...")
        with open(file_name, "w", encoding="utf8") as layout_file:
            json.dump(contents, layout_file, indent="    ")
    elif not changes:
        print("No changes")
    else:
        print("Would have written changes, but requested dry run")
    return changes
