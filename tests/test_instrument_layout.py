"""
Check that instrument layouts are consistent and conform to
schemas
"""

import json

import pytest
from ska_telmodel.schema import validate

from ska_telmodel_data.instrument_layout import (
    read_layout,
    update_receptors_in_file,
)


@pytest.mark.parametrize(
    "input_file,layout_file",
    [
        (
            "inputs/instrument/layout/ska_low_table_10_1_updated.dat",
            "tmdata/instrument/ska1_low/layout/low-layout.json",
        ),
        (
            "inputs/instrument/layout/ska_low_table_10_1_updated.dat",
            "tmdata/instrument/ska1_low/layout/data.json",
        ),
        (
            "inputs/instrument/layout/SKA1_Mid_rev_09.txt",
            "tmdata/instrument/ska1_mid/layout/mid-layout.json",
        ),
    ],
)
def test_instrument_layout_consistency(input_file, layout_file):
    """
    Check that instrument layouts are consistent and conform to
    schemas
    """
    # Check that things are consistent in relation to primary source
    apos_by_name = read_layout(input_file)
    assert not update_receptors_in_file(
        layout_file, apos_by_name, dry_run=True
    )

    # Check that we pass the schema check
    with open(layout_file, "r", encoding="utf8") as layout:
        contents = json.load(layout)
        validate(None, contents)
