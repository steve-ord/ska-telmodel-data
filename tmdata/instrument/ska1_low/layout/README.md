
SKA Low layout
--------------

These files represent the current best understanding for the position
and properties of SKA Low telescope receptors. The current version is
based on SKA1_Low_KCA_Dec_2022_1.kml as provided by Andrea Kuhn on
23/03/2023.

File schemas are documented in telescope model documentation:

https://developer.skao.int/projects/ska-telmodel/en/latest/schemas/ska-telmodel-layout.html

